package kbell;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Server extends Thread implements Constants {
	// 48090
	private static Server inst;
	private boolean terminated = false;
	private ServerSocket server;
	private MessageCallback messageCallback;

	public Timer timer;

	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static volatile List<TcpSocket> sockets = new ArrayList<TcpSocket>();
	private PacketParser packetParser;

	private Server() {
		this.packetParser = new PacketParser();
	}

	public static Server getInst() {
		if (inst == null)
			inst = new Server();
		return inst;
	}

	public void setMessageCallback(MessageCallback messageCallback) {
		this.messageCallback = messageCallback;
	}
	
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	
	
	public static String byteArrayToHexString(byte[] bytes){ 
		
		StringBuilder sb = new StringBuilder(); 
		for(byte b : bytes){ 
			sb.append(String.format("%02X", b&0xff)); 
		} 
		
		return sb.toString(); 
	} 

	public void run() {
		try {
			server = new ServerSocket(TCP_SERVER_PORT);
			server.setReuseAddress(true);

			System.out.println("\n======================= TCP SERVER START  ====================");
			while (!terminated) {
				Socket socket = server.accept();
				socket.setSoLinger(true, 0);
				socket.setSoTimeout(CONNECTION_TIMEOUT);
				// socket.setSoTimeout(CONNECTION_TIMEOUT);// if exceed this timeout -> occured
				// "Read timed out"

				try {

					TcpSocket tcpSocket = new TcpSocket(socket, System.currentTimeMillis(), new ClientCallback() {

						public void onMessage(TcpSocket socket, String message) {
							// not use
						}

						@Override
						public void onMessage(TcpSocket socket, byte[] message, int readCount) {
							
							if(message != null && message.length >0) {
								
								showRecvPacketLog(message,readCount);
								
								if(validate(socket,message,readCount)) { // 패킷 검증
									MessageBean messageBean = parseMessage(socket, message, readCount);
									
									if(messageBean!=null) {

										if(messageBean.message_type == MSG_TYPE_SN_INSERT_REQ) {
											StringBuffer sb= new StringBuffer();
											sb.append("device_id : "+messageBean.device_id);
											sb.append(", latitude : "+messageBean.latitude);
											sb.append(", longitude : "+messageBean.longitude);
											sb.append(", temp : "+messageBean.temperature);
											sb.append(", humi : "+messageBean.humidity);
											sb.append(", PM10_0 : "+messageBean.PM10_0);
											sb.append(", PM2_5 : "+messageBean.PM2_5);
											sb.append(", PM1_0 : "+messageBean.PM1_0);
											sb.append(", O3 : "+messageBean.O3);
											sb.append(", NO2 : "+messageBean.NO2);
											sb.append(", SO2 : "+messageBean.SO2);
											sb.append(", Battery : "+messageBean.Battery);
											System.out.println(format.format(new Date()) + " >> " + sb.toString());
										}
										
										messageCallback.messageRecv(socket, messageBean);
									}else {
										System.out.println("messageBean@@ is null");
									}
								}
							}else {
								
								System.out.println("message## is null");
								
							}
						}

						public void onDisconnect(TcpSocket socket, String message) {
							System.out.println("onDisconnect : " + message);
							messageCallback.disconnect(socket);
							//socket.disconnect();

							synchronized (sockets) {
								sockets.remove(socket);
							}
							

						}

						public void onConnectError(TcpSocket socket, String message) {
							System.out.println("onConnectError " + message);
							messageCallback.disconnect(socket);
							//socket.disconnect();

							synchronized (sockets) {
								sockets.remove(socket);
							}
						}

						public void onConnect(TcpSocket socket) {
							
						}

					});
					tcpSocket.start();

					synchronized (sockets) {
						sockets.add(tcpSocket); // socket management
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println("\n\n======================= TCP SOCKET ACCEPT ==================== ");

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
	public boolean validate(TcpSocket socket,byte[] message, int readCount) {
		if(readCount < 4) { // at least 4 byte are required {startbyte1, startbyte2, message type, payloadLength}
			socket.send("at least 4 byte are required");
			return false;
		}
		
		if(message[0] != START_BYTE1 || message[1] != START_BYTE2) {
			socket.send("START_BYTE to be 0x7f,0x3f  received : "+String.format("%02x", message[0])+","+String.format("%02x", message[1]));
			return false;
		}
		
		return true;
	}
	
	public void showRecvPacketLog( byte[] message,int readCount) {
		
		System.out.println("receive packet size : "+readCount);
		
		byte[] packet = new byte[readCount];
		System.arraycopy(message, 0, packet, 0, readCount);
		
		
		List<String> hexStringList = new ArrayList<String>();
		for(byte b: packet) {
			hexStringList.add(String.format("%02x  ", b&0xff));
		}

		System.out.println("========================= RECV PACKET ========================");
		int i = 0;
		for(String hex : hexStringList) {
			System.out.print(hex);
			i++;
			
			if(i % 16 == 0 ) {
				System.out.print("\n");
			}
		}

		System.out.println("\n==============================================================");
	}

	public MessageBean parseMessage(TcpSocket socket, byte[] message, int readCount) {
		return packetParser.parseMessage(socket, message, readCount);
	}

	public void startExpScheduler() {
		timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				synchronized (sockets) {
					Iterator<TcpSocket> ite = sockets.iterator();
					List<TcpSocket> remove_sockets = new ArrayList<TcpSocket>();

					while (ite.hasNext()) {
						TcpSocket socket = ite.next();
						if (System.currentTimeMillis() - socket.timestamp > CONNECTION_TIMEOUT) {
							System.out.println("CONNECTION_TIME_LIMIT PORT : " + socket.socket.getPort());
							socket.disconnect();
							remove_sockets.add(socket);
							// sockets.remove(socket);
						}
					}

					for (TcpSocket tcpSocket : remove_sockets) {
						sockets.remove(tcpSocket);
					}
				}
			}
		}, 0, CONNECTION_TIMEOUT);
	}

	public void close() {
		inst = null;
		terminated = true;
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
