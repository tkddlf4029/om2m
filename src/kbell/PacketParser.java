package kbell;

public class PacketParser implements Constants {
	
	

	public PacketParser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MessageBean parseMessage(TcpSocket socket, byte[] message, int readCount) {
		
		MessageBean messageBean = new MessageBean(message);
		if(messageBean.message_type == MSG_TYPE_DEV_REG_REQ 
				|| messageBean.message_type == MSG_TYPE_SNC_CREATE_REQ 
				|| messageBean.message_type == MSG_TYPE_SN_INSERT_REQ
				|| messageBean.message_type == MSG_TYPE_SRV_IP_REQ
				|| messageBean.message_type == MSG_TYPE_OTA_INFO_REQ
				|| messageBean.message_type == MSG_TYPE_OTA_SEND_REQ
				|| messageBean.message_type == MSG_TYPE_OTA_FIN_REQ
				) 
		{
			return messageBean;
		}else {
			socket.send("Message type is not support type");
			return null;
		}
		
	}
	
}
