package kbell.ota;
public interface MessageDefine {

	public static final String charset = "utf-8";
	
	// 서버 접속 요청 & ID 발급
	public static final short ID_RSP				= 0x2121; // DEVICE --> SERVER 
	public static final short ID_REQ				= 0x2221; // SERVER --> DEVICE
	
	// 센서 상태정보 수신
	public static final short SENSOR_STATUS_RSP 	= 0x2000; // DEVICE --> SERVER
	
	// 센서 수집정보 수신
	public static final short SENSOR_INFO_RSP 		= 0x2001; // DEVICE --> SERVER
	
	// 디바이스 버전확인 요청 및 결과
	public static final short OTA_UPDATE_INFO_RSP	= 0x1130; // DEVICE --> SERVER
	public static final short OTA_UPDATE_INFO_REQ	= 0x1230; // SERVER --> DEVICE
	
	// 디바이스 펌웨어 업데이트 데이터 요청 및 전송
	public static final short OTA_UPDATE_SEND_RSP	= 0x1132; // DEVICE --> SERVER
	public static final short OTA_UPDATE_SEND_REQ	= 0x1232; // SERVER --> DEVICE
	
	public static final short OTA_UPDATE_FINISH_RSP = 0x1133; // DEVICE --> SERVER
	public static final short OTA_UPDATE_FINISH_REQ = 0x1233; // SERVER --> DEVICE
	
}
