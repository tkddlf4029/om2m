package kbell.ota;
public class OTA_UPDATE_SEND_RSP_T extends MSG_T {

	private String device_id;
	private String version;
	private short mtu_size;
	private short mtu_index;
	
	public void read(byte[] buf) {
		this.buf = buf;
		this.pos = 0;
		
		device_id = readString(8);
		version = readString(8);
		mtu_size = read_short();
		mtu_index = read_short();
	}
	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public short getMtu_size() {
		return mtu_size;
	}
	public void setMtu_size(short mtu_size) {
		this.mtu_size = mtu_size;
	}
	public short getMtu_index() {
		return mtu_index;
	}
	public void setMtu_index(short mtu_index) {
		this.mtu_index = mtu_index;
	}

	@Override
	public String toString() {
		return "OTA_UPDATE_SEND_RSP_T [device_id=" + device_id + ", version=" + version + ", mtu_size=" + mtu_size
				+ ", mtu_index=" + mtu_index + "]";
	}
	
}
