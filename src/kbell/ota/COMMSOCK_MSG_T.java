package kbell.ota;

import kbell.TcpSocket;

public class COMMSOCK_MSG_T extends MSG_T {
	

	public void read(short msg_id, short length, byte[] buf, TcpSocket soc) {
		this.buf = buf;
		this.pos = 0;

//		System.out.println("msg_id: " + msg_id);
		
		switch (msg_id) {
			
			case OTA_UPDATE_INFO_RSP:
				
				OTA_UPDATE_INFO_RSP_T ota_update_info_rsp_t = new OTA_UPDATE_INFO_RSP_T();
				ota_update_info_rsp_t.read(buf);

				
				try {
					OTA_UPDATE_INFO_REQ_T ota_update_info_req_t = new OTA_UPDATE_INFO_REQ_T();
					ota_update_info_req_t.setDevice_id(ota_update_info_rsp_t.device_id);
					ota_update_info_req_t.setVersion("V01.0001");
					ota_update_info_req_t.setFile_size(EnumDefine.fileinfodto.getFile_size());
					ota_update_info_req_t.setFile_crc(EnumDefine.fileinfodto.getFile_crc());
					ota_update_info_req_t.setMtu_size((short) 256);
					ota_update_info_req_t.setMtu_count(EnumDefine.fileinfodto.getMtu_count());
					soc.send(ota_update_info_req_t.write());
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			break;
			
			case OTA_UPDATE_SEND_RSP:
				OTA_UPDATE_SEND_RSP_T ota_update_send_rsp_t = new OTA_UPDATE_SEND_RSP_T();
				ota_update_send_rsp_t.read(buf);
				
				try {
					byte[] send_file_buf = EnumDefine.fileinfodto.getFile_data().get(ota_update_send_rsp_t.getMtu_index());
					
					OTA_UPDATE_SEND_REQ_T ota_update_send_req_t = new OTA_UPDATE_SEND_REQ_T();
					ota_update_send_req_t.setDevice_id(ota_update_send_rsp_t.getDevice_id());
					ota_update_send_req_t.setVersion("V01.0001");
					ota_update_send_req_t.setMtu_size((short) send_file_buf.length);
					ota_update_send_req_t.setMtu_index(ota_update_send_rsp_t.getMtu_index());
					ota_update_send_req_t.setCrc(fn_makeCRC16toShort(send_file_buf));
					ota_update_send_req_t.setData(send_file_buf);
					soc.send(ota_update_send_req_t.write());
					
					logger.info(ota_update_send_req_t.toString());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			break;
			
			case OTA_UPDATE_FINISH_RSP:
				
				OTA_UPDATE_FINISH_RSP_T ota_update_finish_rsp_t = new OTA_UPDATE_FINISH_RSP_T();
				ota_update_finish_rsp_t.read(buf);
				
				OTA_UPDATE_FINISH_REQ_T ota_update_finish_req_t = new OTA_UPDATE_FINISH_REQ_T();
				ota_update_finish_req_t.setDevice_id(ota_update_finish_rsp_t.getDevice_id());
				ota_update_finish_req_t.setVersion("");
				soc.send(ota_update_finish_req_t.write());
				
				
			break;
		}
	}
	
	public short fn_makeCRC16toShort(byte[] bytes) {
		short icrc = 0;
		
		for (byte b : bytes) {
			icrc += ((b & 0xff)); 
		}
		
		return (short)icrc; 
	}
 
	public byte[] write() {
		this.buf = new byte[4096];
		this.pos = 0;

		byte[] ret = new byte[pos];
		System.arraycopy(buf, 0, ret, 0, pos);
		return ret;
	}

}
