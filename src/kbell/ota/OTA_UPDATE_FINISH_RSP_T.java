package kbell.ota;
public class OTA_UPDATE_FINISH_RSP_T extends MSG_T {

	private String device_id;
	private String version;
	
	public void read(byte[] buf) {
		this.buf = buf;
		this.pos = 0;
		
		device_id = readString(8);
		version = readString(8);
	}
	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "OTA_UPDATE_FINISH_RSP_T [device_id=" + device_id + ", version=" + version + "]";
	}
	
}
