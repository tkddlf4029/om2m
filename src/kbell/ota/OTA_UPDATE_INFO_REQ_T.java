package kbell.ota;
public class OTA_UPDATE_INFO_REQ_T extends MSG_T {

	private String device_id;
	private String version;
	private int file_size;
	private short file_crc;
	private short mtu_size;
	private short mtu_count;
	
	public byte[] write() {
		this.buf = new byte[4096];
		this.pos = 0;
		
		write_short(OTA_UPDATE_INFO_REQ);
		write_long(0);
		write_short((short) 26);
		write_string(device_id, 8);
		write_string(version, 8);
		write_int(file_size);
		write_short(file_crc);
		write_short(mtu_size);
		write_short(mtu_count);
		
		byte[] ret = new byte[pos];
		System.arraycopy(buf, 0, ret, 0, pos);
		return ret;
	}
	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public int getFile_size() {
		return file_size;
	}
	public void setFile_size(int file_size) {
		this.file_size = file_size;
	}
	public short getFile_crc() {
		return file_crc;
	}
	public void setFile_crc(short file_crc) {
		this.file_crc = file_crc;
	}
	public short getMtu_size() {
		return mtu_size;
	}
	public void setMtu_size(short mtu_size) {
		this.mtu_size = mtu_size;
	}
	public short getMtu_count() {
		return mtu_count;
	}
	public void setMtu_count(short mtu_count) {
		this.mtu_count = mtu_count;
	}

	@Override
	public String toString() {
		return "OTA_UPDATE_INFO_REQ_T [device_id=" + device_id + ", version=" + version + ", file_size=" + file_size
				+ ", file_crc=" + file_crc + ", mtu_size=" + mtu_size + ", mtu_count=" + mtu_count + "]";
	}
	
}
