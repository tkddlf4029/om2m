package kbell.ota;

import java.util.HashMap;

import com.company.guardian.dto.FileInfoDto;
import com.company.guardian.manager.msg.SENSOR_STATUS_RSP_T;

public class EnumDefine {
	
	public static final int TPKT_LENGTH 				= 4;
	
	public static final int UUID_SIZE 					= 16;
	
	public static final int DEFAULT_BUFFER_SIZE			= 256;
	
	
}
