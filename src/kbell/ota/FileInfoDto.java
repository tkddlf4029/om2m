package kbell.ota;
import java.util.List;

public class FileInfoDto {

	private String file_name;
	private int file_size;
	private short file_crc;
	private short mtu_count;
	private List<byte[]> file_data;
	
	private String version;
	
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public int getFile_size() {
		return file_size;
	}
	public void setFile_size(int file_size) {
		this.file_size = file_size;
	}
	public short getFile_crc() {
		return file_crc;
	}
	public void setFile_crc(short file_crc) {
		this.file_crc = file_crc;
	}
	public short getMtu_count() {
		return mtu_count;
	}
	public void setMtu_count(short mtu_count) {
		this.mtu_count = mtu_count;
	}
	public List<byte[]> getFile_data() {
		return file_data;
	}
	public void setFile_data(List<byte[]> file_data) {
		this.file_data = file_data;
	}
	
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@Override
	public String toString() {
		return "FileInfoDto [file_name=" + file_name + ", file_size=" + file_size + ", file_crc=" + file_crc
				+ ", mtu_count=" + mtu_count + ", file_data=" + file_data + "]";
	}
	
}
