package kbell.ota;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.company.guardian.define.EnumDefine;
import com.company.guardian.dto.FileInfoDto;

public class FileInputOutput implements Runnable {

	@Override
	public void run() {
		File file = new File("E:/key_test/STM32_OTA_BASE.binary");
		
		if(!file.exists()) {
			System.out.println("File Not Found");
		}
		
		System.out.println(" File Create Start !!");
		
		FileInfoDto dto = new FileInfoDto();
		List<byte[]> file_data = new ArrayList<byte[]>();
		
		byte[] buf = new byte[EnumDefine.DEFAULT_BUFFER_SIZE];
		int readBytes;
		short mtu_count = 0;
		
		short icrc = 0;
		
		try {
			FileInputStream fis = new FileInputStream(file);
//			FileOutputStream fos = new FileOutputStream("E:/STM32_OTA_BASE.binary");

//			byte[] buf16 = new byte[16]; // 헥사 값이 16바이트씩, 즉, 한줄씩 저장될 버퍼
//		    int offset = 0; // 번지
//		    int buf16Len;   // 한줄에 들어있는 헥사 값의 개수, 즉, 길이
//		    DataInputStream in = new DataInputStream(fis);
//
//		    while ((buf16Len = in.read(buf16)) > 0) {
//		      System.out.format("%08X:  ", offset); // Offset (번지) 출력
//
//		      // 헥사 구역의 헥사 값 16개 출력 (8개씩 2부분으로)
//		      for (int i = 0; i < buf16Len; i++) {
//		        if (i == 8) System.out.print(" ");    // 8개씩 분리
//		        System.out.format("%02X ", buf16[i]); // 헥사 값 출력
//		      }
//
//		      // 한 줄이 16 바이트가 되지 않을 때, 헥사 부분과 문자 부분 사이에 공백들 삽입
//		      for (int i = 0; i <= (16 - buf16Len) * 3; i++)
//		        System.out.print(" ");
//		      if (buf16Len < 9) System.out.print(" "); // 한줄이 9바이트보다 적을 때는 한칸 더 삽입
//
//
//		      // 문자 구역 출력
//		      for (int i = 0; i < buf16Len; i++) {
//		        if (buf16[i] >= 0x20 && buf16[i] <= 0x7E) // 특수 문자 아니면 출력
//		          System.out.format("%c", buf16[i]);
//		        else System.out.print("."); // 특수문자, 그래픽문자 등은 마침표로 출력
//		      }
//
//		      offset += 16; // 번지 값을 16 증가
//		      System.out.println(); // 줄바꿈
//		    }
//
//		    if (offset == 0) System.out.format("%08X:  ", offset); // 0바이트 파일일 경우 처리
//		    in.close();
			
			while((readBytes = fis.read(buf)) > 0) {
//				fos.write(buf, 0, readBytes);
				
				byte[] new_buf = new byte[readBytes];
				System.arraycopy(buf, 0, new_buf, 0, readBytes);
				file_data.add(new_buf);
				
				for(byte b : new_buf) {
					icrc += ((b & 0xff));
				}
				
				mtu_count++;
			}
			
			dto.setFile_name(file.getName());
			dto.setFile_size((int) file.length());
			dto.setFile_crc(icrc);
			dto.setMtu_count(mtu_count);
			dto.setFile_data(file_data);
			
			System.out.println(dto.toString());
			
			fis.close();
//			fos.close();
			
			System.out.println(" File Create !! ");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
