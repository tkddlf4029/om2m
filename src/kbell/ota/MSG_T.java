package kbell.ota;
import java.io.UnsupportedEncodingException;

import kbell.Constants;

public class MSG_T implements Constants {
	protected byte[] buf;

	protected int pos;

	protected int readInt() {
//		return     (buf[pos++]        & 0xff) | ((buf[pos++] <<  8) & 0xff00) | ((buf[pos++] << 16) & 0xff0000) |  (buf[pos++] << 24);
		return     ((buf[pos++] << 24) | ((buf[pos++] << 16) & 0xff0000) | ((buf[pos++] <<  8) & 0xff00) |  buf[pos++]        & 0xff);
	}

	public synchronized long read_uint() {
		Long s;
		s =
				(((long) buf[pos++]) & 0xffL) |
				((((long) buf[pos++]) << 8) & 0xff00L) |
				((((long) buf[pos++]) << 16) & 0xff0000L) |
				((((long) buf[pos++]) << 24) & 0xff000000L);
		/*
            s =
                ((((long) buf[pos_++]) << 24) & 0xff000000L) |
                ((((long) buf[pos_++]) << 16) & 0xff0000L) |
                ((((long) buf[pos_++]) << 8) & 0xff00L) |
                (((long) buf[pos_++]) & 0xffL);
		 */

		return s;
	}

	public synchronized long read_int32() {
		long s = 0;

		s = (long)((buf[pos++] << 24) | (buf[pos++] << 16) & 0xff0000) | ((buf[pos++] << 8) & 0xff00 | (buf[pos++] & 0xff));

		return s;
	}


	public synchronized short read_short() {
		try {
			//			pos += pos & 0x1;  //홀수 위치 shift 시켜주는 거라 생각하고 주석처리함
//			return (short) ((buf[pos++] & 0xff) | (buf[pos++] << 8));
			return (short) ((buf[pos++] << 8) | (buf[pos++] & 0xff));

		}catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public synchronized byte read_byte() {
		try {
			return (byte) ((buf[pos++] & 0xff));

		}catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public synchronized int read_uint16() {
		try {

			return (int) ((buf[pos++] << 8) & 0xff00 | (buf[pos++] & 0xff));

		}catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public synchronized short read_int16() {
		try {

			return (short) ((buf[pos++] & 0xff) | (buf[pos++] << 8) & 0xff00);

		}catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public synchronized int read_uint32() {
		Integer s = 0;

		s =  ((buf[pos++] << 24) | (buf[pos++] << 16) & 0xff0000) | ((buf[pos++] << 8) & 0xff00 | (buf[pos++] & 0xff));

		return s;
	}

	protected double readDouble() {
		return Double.longBitsToDouble(readLong());
	}

	protected long readLong() {
		return     ((long) buf[pos++]        & 0xffL)
				| (((long) buf[pos++] <<  8) & 0xff00L)
				| (((long) buf[pos++] << 16) & 0xff0000L)
				| (((long) buf[pos++] << 24) & 0xff000000L)
				| (((long) buf[pos++] << 32) & 0xff00000000L)
				| (((long) buf[pos++] << 40) & 0xff0000000000L)
				| (((long) buf[pos++] << 48) & 0xff000000000000L)
				|  ((long) buf[pos++] << 56);
	}

	protected byte[] readBuffer(int len) {
		len = Math.min(buf.length - pos, len);

		byte[] tmp = new byte[len];
		System.arraycopy(buf, pos, tmp, 0, len);
		pos += len;

		return tmp;
	}

	protected String readString(int len) {
		len = Math.min(buf.length - pos, len);

		byte[] tmp = new byte[len];
		System.arraycopy(buf, pos, tmp, 0, len);
		pos += len;

		try {
			return new String(tmp, charset).trim();
		} catch (UnsupportedEncodingException e) {
			return new String(tmp).trim();
		}
	}

	protected void writeInt(int val) {
		buf[pos++] = (byte) val;
		buf[pos++] = (byte) (val >> 8);
		buf[pos++] = (byte) (val >> 16);
		buf[pos++] = (byte) (val >> 24);
	}

	protected void writeString(String val, int len) {
		byte[] tmp;
		try {
			tmp = val.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			tmp = val.getBytes();
		}
		System.arraycopy(tmp, 0, buf, pos, tmp.length);
		for (int i = tmp.length; i < len; i++) {
			buf[pos + i] = 0;
		}
		pos += len;
	}

	protected void writeBuffer(byte[] val, int len) {
		System.arraycopy(val, 0, buf, pos, len);
		pos+= len;
	}

	protected byte[] readByteArray(int len){
		byte[] val = new byte[len];

		System.arraycopy(val, 0, buf, pos, len);
		return val;
	}

	public synchronized short read_uchar() {
		return (short) read_char();
	}

	public synchronized char read_char() {
		return (char) (buf[pos++] & 0xff);
	}

	public synchronized int read_ushort() {
		Integer s = 0;

		//		s = ((buf[pos++] & 0xff) | (buf[pos++] << 8));
		s = ((buf[pos++] << 8) | (buf[pos++] & 0xff));

		return s;
	}

	public synchronized void write_boolean(boolean value) {
		buf[pos++] = value ? (byte) 1 : (byte) 0;
	}

	public synchronized void write_char(char value) {
		if (value > 255) {
		}
		buf[pos++] = (byte) value;
	}

	public synchronized void write_uchar(short value) {
		write_char((char) value);
	}

	public synchronized void write_wchar(char value) {
		write_short((short) value);
	}

	public synchronized void write_octet(byte value) {
		buf[pos++] = value;
	}

	public synchronized void write_short(short value) {
		buf[pos++] = (byte) (value >> 8);
		buf[pos++] = (byte) value;
	}

	public synchronized void write_short_reverse(short value) {
		//Byte Ordering 함.
		final int cmod2 = pos & 0x1;
		if (cmod2 != 0) {
			pos += cmod2;
		}
		buf[pos++] = (byte) value;
		buf[pos++] = (byte) (value >> 8);
	}

	public synchronized void write_ushort(short value) {
		write_short(value);
	}

	public synchronized void write_int(int value) {
		buf[pos++] = (byte) (value >> 24);
		buf[pos++] = (byte) (value >> 16);
		buf[pos++] = (byte) (value >> 8);
		buf[pos++] = (byte) value;
	}

	public synchronized void write_ushort(int value) {
		buf[pos++] = (byte) (value >> 8);
		buf[pos++] = (byte) value;
	}

	public synchronized void write_uint(long value) {
		buf[pos++] = (byte) (value >> 24);
		buf[pos++] = (byte) (value >> 16);
		buf[pos++] = (byte) (value >> 8);
		buf[pos++] = (byte) value;
	}

	public synchronized void write_uint_reverse(long value) {
		buf[pos++] = (byte) value;
		buf[pos++] = (byte) (value >> 8);
		buf[pos++] = (byte) (value >> 16);
		buf[pos++] = (byte) (value >> 24);
	}

	public synchronized void write_long(long value) {
		buf[pos++] = (byte) (value >> 56);
		buf[pos++] = (byte) (value >> 48);
		buf[pos++] = (byte) (value >> 40);
		buf[pos++] = (byte) (value >> 32);
		buf[pos++] = (byte) (value >> 24);
		buf[pos++] = (byte) (value >> 16);
		buf[pos++] = (byte) (value >> 8);
		buf[pos++] = (byte) value;
	}
	
	public synchronized void write_long_reverse(long value) {
		
//		final int cmod8 = pos & 0x7;
//		if (cmod8 != 0) {
//			pos += 8 - cmod8;
//		}
		buf[pos++] = (byte) value;
		buf[pos++] = (byte) (value >> 8);
		buf[pos++] = (byte) (value >> 16);
		buf[pos++] = (byte) (value >> 24);

		buf[pos++] = (byte) (value >> 32);
		buf[pos++] = (byte) (value >> 40);
		buf[pos++] = (byte) (value >> 48);
		buf[pos++] = (byte) (value >> 56);
		
		
	}

	public synchronized void write_ulong(long value) {
		write_long(value);
	}

	public synchronized void write_float(float value) {
		write_long(Float.floatToIntBits(value));
	}

	public synchronized void write_double(double value) {

//		System.out.println("Double.doubleToLongBits(value) :" + Double.doubleToLongBits(value));

		write_long(Double.doubleToLongBits(value));
	}

	public synchronized void write_double_reverse(double value) {
		write_long_reverse(Double.doubleToLongBits(value));
	}

	public synchronized void write_string(String value) {
		final int len = value.length();
		byte[] bytes = new byte[len];
		for (int i = 0; i < len; i++) {
			char ch = value.charAt(i);
			if (ch > 255) {
			}
			bytes[i] = (byte) ch;
		}

//		write_ulong(len + 1);

		System.arraycopy(bytes, 0, buf, pos, len);

		pos += len;
//		buf[pos++] = 0;
	}
	
	public synchronized void write_hexstring(String value) {
		final int len = value.length();
//		System.out.println("value.length = " + len);
//		byte[] bytes= new BigInteger(value, 16).toByteArray();
//		System.out.println("1_bytes.length = " + bytes.length);
//		
//		if(bytes[0] == 0){
//			byte[] tmp = new byte[bytes.length - 1];
//			System.arraycopy(bytes, 1, tmp, 0, tmp.length);
//			System.out.println("tmp.length = " + tmp.length );
//			bytes = tmp;
//			System.out.println("2_bytes.length = " + bytes.length);
//			System.arraycopy(tmp, 0, buf, pos, tmp.length);
//			pos += tmp.length-1;
//		}
		
		byte[] data = new byte[len / 2];
		for(int i=0; i<len; i+=2){
			data[i/2] = (byte)((Character.digit(value.charAt(i), 16) << 4) + (Character.digit(value.charAt(i+1), 16)));
		}
		
		System.arraycopy(data, 0, buf, pos, data.length);
		pos += data.length;
//		buf[pos++] = 0;
	}

	public synchronized void write_string(String value, int len) {
		byte[] bytes = value.getBytes();

		//		for (int i = 0; i < len; i++) {
		//			char ch = value.charAt(i);
		//			if (ch > 255) {
		//			}
		//			bytes[i] = (byte) ch;
		//		}
		//
		//		write_ulong(len + 1);

		System.arraycopy(bytes, 0, buf, pos, bytes.length);

		for(int i = bytes.length; i<len; i++){
			buf[pos+i] = 0;
		}
		pos += len;
		//		buf[pos++] = 0;
	}

	public synchronized void write_wstring(String value) {
		final int len = value.length();
		write_ushort(len * 2 + 1);

		for (int i = 0; i < len; i++) {
			char ch = value.charAt(i);
			buf[pos++] = (byte) (ch >> 8);
			buf[pos++] = (byte) ch;
		}

		buf[pos++] = 0;
		buf[pos++] = 0;
	}

	public synchronized void write_boolean_array(Boolean[] value, int offset, int length) {
		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = value[i] ? (byte) 1 : (byte) 0;
		}
	}

	public synchronized void write_char_array(Character[] value, int offset, int length) {
		for (int i = offset; i < offset + length; i++) {
			if (value[i] > 255) {
			}
			buf[pos++] = (byte) value[i].charValue();
		}
	}

	public synchronized void write_char_array(char[] value, int offset, int length) {
		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = (byte) value[i];
		}
	}

	public synchronized void write_uchar_array(Character[] value, int offset, int length) {
		write_char_array(value, offset, length);
	}

	public synchronized void write_wchar_array(Character[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod2 = pos & 0x1;
		if (cmod2 != 0) {
			pos += cmod2;
		}

		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = (byte) (value[i] >> 8);
			buf[pos++] = (byte) value[i].charValue();
		}
	}

	public synchronized void write_octet_array(Byte[] value, int offset, int length) {
		System.arraycopy(value, offset, buf, pos, length);
		pos += length;
	}

	public synchronized void write_short_array(Short[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod2 = pos & 0x1;
		if (cmod2 != 0) {
			pos += cmod2;
		}
		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = (byte) (value[i] >> 8);
			buf[pos++] = (byte) value[i].shortValue();
		}
	}

	public synchronized void write_ushort_array(Short[] value, int offset, int length) {
		write_short_array(value, offset, length);
	}

	public synchronized void write_int_array(Integer[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod4 = pos & 0x3;
		if (cmod4 != 0) {
			pos += 4 - cmod4;
		}
		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = (byte) (value[i] >> 24);
			buf[pos++] = (byte) (value[i] >> 16);
			buf[pos++] = (byte) (value[i] >> 8);
			buf[pos++] = (byte) value[i].intValue();
		}
	}

	public synchronized void write_uint_array(Long[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod8 = pos & 0x7;
		if (cmod8 != 0) {
			pos += 8 - cmod8;
		}
		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = (byte) (value[i] >> 24);
			buf[pos++] = (byte) (value[i] >> 16);
			buf[pos++] = (byte) (value[i] >> 8);
			buf[pos++] = (byte) value[i].longValue();
		}
	}

	public synchronized void write_uint_array(long[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod8 = pos & 0x7;
		if (cmod8 != 0) {
			pos += 8 - cmod8;
		}
		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = (byte) (value[i] >> 24);
			buf[pos++] = (byte) (value[i] >> 16);
			buf[pos++] = (byte) (value[i] >> 8);
			buf[pos++] = (byte) value[i];
		}
	}

	public synchronized void write_long_array(Long[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod8 = pos & 0x7;
		if (cmod8 != 0) {
			pos += 8 - cmod8;
		}
		for (int i = offset; i < offset + length; i++) {
			buf[pos++] = (byte) (value[i] >> 56);
			buf[pos++] = (byte) (value[i] >> 48);
			buf[pos++] = (byte) (value[i] >> 40);
			buf[pos++] = (byte) (value[i] >> 32);
			buf[pos++] = (byte) (value[i] >> 24);
			buf[pos++] = (byte) (value[i] >> 16);
			buf[pos++] = (byte) (value[i] >> 8);
			buf[pos++] = (byte) value[i].longValue();
		}
	}

	public synchronized void write_ulong_array(Long[] value, int offset, int length) {
		write_long_array(value, offset, length);
	}

	public synchronized void write_float_array(Float[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod4 = pos & 0x3;
		if (cmod4 != 0) {
			pos += 4 - cmod4;
		}
		for (int i = offset; i < offset + length; i++) {
			int v = Float.floatToIntBits(value[i]);

			buf[pos++] = (byte) (v >> 24);
			buf[pos++] = (byte) (v >> 16);
			buf[pos++] = (byte) (v >> 8);
			buf[pos++] = (byte) v;
		}
	}

	public synchronized void write_double_array(Double[] value, int offset, int length) {
		if (length <= 0) {
			return;
		}

		final int cmod8 = pos & 0x7;
		if (cmod8 != 0) {
			pos += 8 - cmod8;
		}
		for (int i = offset; i < offset + length; i++) {
			long v = Double.doubleToLongBits(value[i]);

			buf[pos++] = (byte) (v >> 56);
			buf[pos++] = (byte) (v >> 48);
			buf[pos++] = (byte) (v >> 40);
			buf[pos++] = (byte) (v >> 32);
			buf[pos++] = (byte) (v >> 24);
			buf[pos++] = (byte) (v >> 16);
			buf[pos++] = (byte) (v >> 8);
			buf[pos++] = (byte) v;
		}
	}

	public synchronized void _writeEndian() {
		write_boolean(false); // false means big endian
	}

	// Needed for classes in other packages
	public synchronized byte[] _buffer() {
		return buf;
	}

	// Needed for classes in other packages
	public synchronized int _count() {
		return pos;
	}
	
	public static String data_array_tohexstring(byte[] bytes){ 
		StringBuilder sb = new StringBuilder(); 
		for(byte b : bytes){ 
			sb.append(String.format("%02X", b&0xff) + " "); 
		} 
		return sb.toString(); 
	} 
	
}

