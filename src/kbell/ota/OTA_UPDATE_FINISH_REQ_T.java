package kbell.ota;

public class OTA_UPDATE_FINISH_REQ_T extends MSG_T {

	private String device_id;
	private String version;
	
	public byte[] write() {
		this.buf = new byte[4096];
		this.pos = 0;
		
		write_short(OTA_UPDATE_FINISH_REQ);
		write_long(0);
		write_short((short) 16);
		write_string(device_id, 8);
		write_string(version, 8);
		
		byte[] ret = new byte[pos];
		System.arraycopy(buf, 0, ret, 0, pos);
		return ret;
	}
	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "OTA_UPDATE_FINISH_REQ_T [device_id=" + device_id + ", version=" + version + "]";
	}
	
}
