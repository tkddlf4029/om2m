package kbell.ota;
import java.util.Arrays;

public class OTA_UPDATE_SEND_REQ_T extends MSG_T {

	private String device_id;
	private String version;
	private short mtu_size;
	private short mtu_index;
	private short crc;	
	private byte[] data;
	
	public byte[] write() {
		this.buf = new byte[4096];
		this.pos = 0;
		
		write_short(OTA_UPDATE_SEND_REQ);
		write_long(0);
		write_short((short) (22 + data.length));
		write_string(device_id, 8);
		write_string(version, 8);
		write_short(mtu_size);
		write_short(mtu_index);
		write_short(crc);
		writeBuffer(data, data.length);
		
		byte[] ret = new byte[pos];
		System.arraycopy(buf, 0, ret, 0, pos);
		return ret;
	}
	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public short getMtu_size() {
		return mtu_size;
	}
	public void setMtu_size(short mtu_size) {
		this.mtu_size = mtu_size;
	}
	public short getMtu_index() {
		return mtu_index;
	}
	public void setMtu_index(short mtu_index) {
		this.mtu_index = mtu_index;
	}
	public short getCrc() {
		return crc;
	}
	public void setCrc(short crc) {
		this.crc = crc;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		;
		return "OTA_UPDATE_SEND_REQ_T [device_id=" + device_id + ", version=" + version + ", mtu_size=" + mtu_size
				+ ", mtu_index=" + mtu_index + ", crc=" + crc + ", data: " + data_array_tohexstring(data) + "]";
	}
	
}
