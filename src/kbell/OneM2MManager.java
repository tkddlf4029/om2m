package kbell;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

public class OneM2MManager implements Constants {
	public static String RESULT_CREATED = "Created";
	public static String RESULT_CONFLICT = "Conflict";
	public String base_url;

	public OneM2MManager(String base_url) {
		this.base_url = base_url;
	}

	public void sendOneM2M(TcpSocket socket, MessageBean om2mBean,ResponseBean responseBean) {
		HttpURLConnection con = null;
		BufferedReader br = null;
		try {
			URL url = null;
			switch (om2mBean.message_type) {
			case MSG_TYPE_DEV_REG_REQ:
				url = new URL(base_url+"?rcn=0");
				break;
			case MSG_TYPE_SNC_CREATE_REQ:
				url = new URL(base_url+"/"+String.format("sc_bic_%04d",om2mBean.device_id)+"?rcn=0");
				break;
			case MSG_TYPE_SN_INSERT_REQ:
				url = new URL(base_url+"/"+String.format("sc_bic_%04d",om2mBean.device_id)+"/sensor?rcn=0");
				break;
			}
			
			System.out.println("url : "+ url.toString());
			
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setConnectTimeout(3000);
			con.setReadTimeout(3000);
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setDefaultUseCaches(false);

			con.setRequestProperty("X-M2M-RI", om2mBean.resource_id + "");
			con.setRequestProperty("X-M2M-Origin", om2mBean.resource_id + "");
			con.setRequestProperty("Content-Type", "application/json;ty=" + om2mBean.ty);

			JSONObject oneM2MRequestBody = om2mBean.getReqBody();

			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
			wr.write(oneM2MRequestBody.toString());
			wr.flush();

			StringBuilder sb = new StringBuilder();
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line).append("\n");
				}
				//socket.send("OM2M HTTP Response  : " + sb.toString());
				System.out.println("OM2M HTTP Response : "+ sb.toString());
			} else {
				//socket.send("OM2M HTTP Response : " + con.getResponseMessage());
				System.out.println("OM2M HTTP Response : "+ con.getResponseMessage());
			}
			
		} catch (Exception e) {
			//socket.send(e.toString());
			System.out.println(e.toString());
		} finally {
			socket.send(responseBean.getBytes(om2mBean.message_type));
			
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(con !=null) {
				con.disconnect();
			}
			//socket.disconnect();
		}

	}

//	public static void post(String strUrl, String jsonMessage) {
//		try {
//			URL url = new URL(strUrl);
//			HttpURLConnection con = (HttpURLConnection) url.openConnection();
//			con.setConnectTimeout(5000);
//			con.setReadTimeout(5000); 
//			con.setRequestMethod("POST");
//			String unique_key = "abcdefg"; 
//			con.setRequestProperty("X-M2M-RI", unique_key);
//			con.setRequestProperty("X-M2M-Origin", unique_key);
//			con.setRequestProperty("Content-Type", "application/json;ty=2");
//			con.setRequestProperty("Accept","*/*");
//			con.setDoInput(true);
//			con.setDoOutput(true); 
//			con.setUseCaches(false);
//			con.setDefaultUseCaches(false);
//			
//			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//			wr.write(jsonMessage); 
//			wr.flush();
//
//			StringBuilder sb = new StringBuilder();
//			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
//				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
//				String line;
//				while ((line = br.readLine()) != null) {
//					sb.append(line).append("\n");
//				}
//				br.close();
//				System.out.println("Response HTTP_OK : " + sb.toString());
//			} else {
//				System.out.println("Response ELSE : " + con.getResponseMessage());
//			}
//		} catch (Exception e) {
//			
//		}
//	}

//	public static void get(String strUrl) {
//	try {
//		URL url = new URL(strUrl);
//		HttpURLConnection con = (HttpURLConnection) url.openConnection();
//		con.setConnectTimeout(5000); // ������ ����Ǵ� Timeout �ð� ����
//		con.setReadTimeout(5000); // InputStream �о� ���� Timeout �ð� ����
//		//con.addRequestProperty("x-api-key", RestTestCommon.API_KEY); // key�� ����
//
//		con.setRequestMethod("GET");
//
//		con.setDoOutput(false);
//
//		StringBuilder sb = new StringBuilder();
//		if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
//			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
//			String line;
//			while ((line = br.readLine()) != null) {
//				sb.append(line).append("\n");
//			}
//			br.close();
//			System.out.println("" + sb.toString());
//		} else {
//			System.out.println(con.getResponseMessage());
//		}
//
//	} catch (Exception e) {
//		System.err.println(e.toString());
//	}
//}

}
