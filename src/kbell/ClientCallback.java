package kbell;


public interface ClientCallback {
	
	void onMessage(TcpSocket socket, String message);
	
	void onMessage(TcpSocket socket, byte[] message,int readCount);

	void onConnect(TcpSocket socket);

	void onDisconnect(TcpSocket socket, String message);

	void onConnectError(TcpSocket socket, String message);
}
