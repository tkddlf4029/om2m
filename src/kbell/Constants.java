package kbell;

public interface Constants {
	

	public static final int CONNECTION_TIMEOUT = 60000;

	public static final int TCP_SERVER_PORT = 6001;
	//public static final int TCP_SERVER_PORT = 4600;
	//public static final int TCP_SERVER_PORT = 8080;
	
	public static enum REQ{
		MSG_TYPE_DEV_REG_REQ,
		MSG_TYPE_SNC_CREATE_REQ,
		MSG_TYPE_SN_INSERT_REQ};

	
	public static final byte START_BYTE1 = (byte)0x7F;
	public static final byte START_BYTE2 = (byte)0x3F;
	
	public static final byte MSG_TYPE_DEV_REG_REQ = (byte)0x01; // 공유시설물 등록 요청
	public static final byte MSG_TYPE_DEV_REG_RES = (byte)0x02; // 공유시설물 등록 응답
	public static final byte MSG_TYPE_SNC_CREATE_REQ = (byte)0x04; // 공유시설물 센서 리소스 생성 요청
	public static final byte MSG_TYPE_SNC_CREATE_RES = (byte)0x08; // 공유시설물 센서 리스트 생성 응답
	public static final byte MSG_TYPE_SN_INSERT_REQ = (byte)0x10; // 공유시설물 센서 데이터 입력 요청
	public static final byte MSG_TYPE_SN_INSERT_RES = (byte)0x11; // 공유시설물 센서 데이터 입력 응답
	public static final byte MSG_TYPE_SRV_IP_REQ = (byte)0x20; // 아이피 요청
	public static final byte MSG_TYPE_SRV_IP_RSP = (byte)0x21; // 아이피 응답

	

	public static final int OTA_VERSION_NAEM_LENGTH = 8; // V00.0000 
	public static final byte MSG_TYPE_OTA_INFO_REQ = (byte)0x30; 
	public static final byte MSG_TYPE_OTA_INFO_RSP = (byte)0x31; 
	public static final byte MSG_TYPE_OTA_SEND_REQ = (byte)0x32; 
	public static final byte MSG_TYPE_OTA_SEND_RSP = (byte)0x33; 
	public static final byte MSG_TYPE_OTA_FIN_REQ = (byte)0x34; 
	public static final byte MSG_TYPE_OTA_FIN_RSP = (byte)0x35; 
	
	
	public static final int TY_AE = 2; 
	public static final int TY_CON = 3; 
	public static final int TY_CIN = 4; 
	
	
	

	public static final String charset="utf-8";
	// 디바이스 버전확인 요청 및 결과
	public static final short OTA_UPDATE_INFO_RSP	= 0x1130; // DEVICE --> SERVER
	public static final short OTA_UPDATE_INFO_REQ	= 0x1230; // SERVER --> DEVICE
	
	// 디바이스 펌웨어 업데이트 데이터 요청 및 전송
	public static final short OTA_UPDATE_SEND_RSP	= 0x1132; // DEVICE --> SERVER
	public static final short OTA_UPDATE_SEND_REQ	= 0x1232; // SERVER --> DEVICE
	
	public static final short OTA_UPDATE_FINISH_RSP = 0x1133; // DEVICE --> SERVER
	public static final short OTA_UPDATE_FINISH_REQ = 0x1233; // SERVER --> DEVICE
	
	public static final int TPKT_LENGTH 				= 4;
	public static final int UUID_SIZE 					= 16;
	public static final int DEFAULT_BUFFER_SIZE			= 256;
	
	
}
