package kbell;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MQTT {

	private final String MQTT_SERVER_IP = "tcp://192.168.3.197:1883";
	private final String CLINET_ID = "";
	private final String USER_NAME = "";
	private final String PASSWORD = "";
	private static int QOS = 0;
	private static MqttAsyncClient Client;
	private static MqttMessage message = new MqttMessage();
	private static MemoryPersistence persistence;
	private static MqttConnectOptions connOpts;
	private MqttCallback callback;

	public MQTT() {
	}

	public void setCallback(MqttCallback callback) {
		this.callback = callback;
	}

	public void connect(IMqttActionListener listener) {
		persistence = new MemoryPersistence();
		try {
			Client = new MqttAsyncClient(MQTT_SERVER_IP, CLINET_ID, persistence);
			Client.setCallback(callback);

			connOpts = new MqttConnectOptions();
			connOpts.setAutomaticReconnect(true);
			connOpts.setCleanSession(true);
			connOpts.setMaxInflight(256);

			// if(Client_ID!=null && Passwd != null){
			// connOpts.setUserName(this.UserName);
			// connOpts.setPassword(this.Passwd.toCharArray());
			// }

			System.out.println("\n======================= MQTT CLIENT START  ====================");
			System.out.println("Connecting to broker: " + MQTT_SERVER_IP);
			Client.connect(connOpts, listener);

		} catch (MqttException me) {
			System.err.println(me.getMessage());
		}
	}

	public void disconnect() {
		try {
			Client.disconnect();
			Client.close();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	int i;
	
	public void publish(String topic, String msg) {
		message.setQos(QOS);
		i++;
		System.out.println("Publish SEQ : "+ i);
		try {
			message.setPayload(msg.getBytes());
			Client.publish(topic, message);
		}catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}

	}

	String TOPIC_TX = "application/+/device/+/tx";
	String TOPIC_RX = "application/+/device/+/rx";

	public void subscribe() {
		try {
			Client.subscribe(TOPIC_RX,QOS);
			//Client.subscribe(TOPIC_TX,QOS);

		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

}
