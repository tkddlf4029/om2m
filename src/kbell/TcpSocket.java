package kbell;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class TcpSocket extends Thread {

	public static final int TCP_MSG_TYPE_HEARTBEAT = 100;
	public static final int HEARTBEAT_TIMER_PERIOD = 5000;

	public Socket socket;
	private InputStream socketInput;
	private OutputStream socketOutput;
	private ClientCallback listener;
	public long timestamp;

	public TcpSocket(Socket socket, long timestamp, ClientCallback listener) {
		this.socket = socket;
		this.timestamp = timestamp;
		this.listener = listener;
	}

	@Override
	public void run() {

		// TODO Auto-generated method stub
		try {
			socketOutput = socket.getOutputStream();
			socketInput = socket.getInputStream();
			new ReceiveThread().start();
			if (listener != null)
				listener.onConnect(this);

		} catch (IOException e) {
			if (listener != null)
				listener.onConnectError(this, e.getMessage());
			//e.printStackTrace();
		} catch (Exception e) {
			if (listener != null)
				listener.onConnectError(this, e.getMessage());
			//e.printStackTrace();
		}
	}

	private class ReceiveThread extends Thread implements Runnable {
		public void run() {
			String message = null;

			int charsRead;
			byte[] buffer = new byte[1024];

			try {

				while ((charsRead = socketInput.read(buffer)) != -1) {
					message = new String(buffer, 0, charsRead);
					if (charsRead > 0) {
						if (listener != null) {
							listener.onMessage(TcpSocket.this, message);
							listener.onMessage(TcpSocket.this, buffer, charsRead);
						} else {
							send("Listener is Null");
						}
					} else {
						send("Payload is Empty");
					}

				}

				if (listener != null)
					listener.onDisconnect(TcpSocket.this, "ReceiveThread read -1");
			} catch (Exception e) {
				if (listener != null)
					listener.onDisconnect(TcpSocket.this, "@@@1"+e.getMessage());
				//e.printStackTrace();
			}

		}
	}

	public void send(String message) {
		try {
			socketOutput.write(message.getBytes());
			socketOutput.flush();
		} catch (Exception e) {
			if (listener != null)
				listener.onDisconnect(TcpSocket.this,"@@@2"+ e.getMessage());
			//e.printStackTrace();
		}
	}

	public void send(byte[] message) {
		try {
			socketOutput.write(message);
			socketOutput.flush();
		} catch (Exception e) {
			if (listener != null)
				listener.onDisconnect(this,"@@@2"+ e.getMessage());
			//e.printStackTrace();
		}
	}

	public void disconnect() {
		try {
			if (socketInput != null) {
				socketInput.close();
			}

			if (socketOutput != null) {
				socketOutput.close();
			}

			if (socket != null) {
				socket.close();
			}

		} catch (Exception e) {
			
			/*
			 * if (listener != null) listener.onDisconnect(this, "@@@3"+e.getMessage());
			 */
			//e.printStackTrace();
			
		} finally {
			if (listener != null)
				listener.onDisconnect(this, "disconnect");
			this.interrupt();
		}
	}

}