package kbell;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import kbell.ota.EnumDefine;
import kbell.ota.FileInfoDto;

public class Main implements Constants {

	static String ONEM2M_SERVER_URL = "http://kbell.co.kr:7579/Mobius";
	static String PROPERTIE_FILE_LOCATION = "./server.prop";
	static Properties properties = new Properties();
	static final NavigableMap<Integer, PropBean> prop_map = new TreeMap<Integer, PropBean>();
	static final Map<TcpSocket, FileInfoDto> firmware_map = new HashMap<TcpSocket, FileInfoDto>();
	static Object lock = new Object();

	public static MQTT mqttClient;

	public static String tiemStamp() {
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// System.out.println(format.format(now)); // 20090529
		return format.format(now);
	}

	public static void main(String[] args) {



		// args ::: http://kbell.co.kr:7579/Mobius ./server.prop

		// java -jar om2m_tcpserver.jar http://kbell.co.kr:7579/Mobius 2>&1 >
		// om2m_tcpserver.log &
		// 너무 많은 발행이 진행 중임 (32202)
		
		// LTE
		if (args.length == 2) {
			ONEM2M_SERVER_URL = args[0];
			PROPERTIE_FILE_LOCATION = args[1];

			setProp(); // init server ip and port properties from server.prop file
			startFileWatcherTimer(); // server.prop file watcher timer

			OneM2MManager oneM2MManager = new OneM2MManager(ONEM2M_SERVER_URL);
			ConnectionManager connectionManager = new ConnectionManager(new MessageCallback() {

				@Override
				public void messageRecv(TcpSocket socket, MessageBean messageBean) {
					// received om2m packet from TCP socket server
					PropBean propBean = null;
					ResponseBean responseBean = new ResponseBean();
					byte response_type = 0;

					switch (messageBean.message_type) {
					case MSG_TYPE_DEV_REG_REQ:
					case MSG_TYPE_SNC_CREATE_REQ:
					case MSG_TYPE_SN_INSERT_REQ:
						if (messageBean.message_type == MSG_TYPE_DEV_REG_REQ) {
							response_type = MSG_TYPE_DEV_REG_RES;
						} else if (messageBean.message_type == MSG_TYPE_SNC_CREATE_REQ) {
							response_type = MSG_TYPE_SNC_CREATE_RES;
						} else if (messageBean.message_type == MSG_TYPE_SN_INSERT_REQ) {
							response_type = MSG_TYPE_SN_INSERT_RES;
						}

						synchronized (lock) {
							propBean = getProp(messageBean.device_id);
							responseBean.setMessage_type(response_type);
							responseBean.setServer_ip(propBean.getIntServer_ip());
							responseBean.setPort(propBean.getPort());
							responseBean.setReturn_value( (short) 0);
							// System.out.println("ResponseBean : " + responseBean.toString());
							System.out.println("PropBean : " + propBean.toString());
							oneM2MManager.sendOneM2M(socket, messageBean, responseBean);
						}
						break;

					case MSG_TYPE_SRV_IP_REQ:
						response_type = MSG_TYPE_SRV_IP_RSP;

						synchronized (lock) {
							propBean = getProp(messageBean.device_id);
							responseBean.setMessage_type(response_type);
							responseBean.setServer_ip(propBean.getIntServer_ip());
							responseBean.setPort(propBean.getPort());
							responseBean.setReturn_value( (short) 0);
							System.out.println("PropBean : " + propBean.toString());
							socket.send(responseBean.getBytes(messageBean.message_type));
						}
						break;
					case MSG_TYPE_OTA_INFO_REQ:
						response_type = MSG_TYPE_OTA_FIN_RSP;
						FileInfoDto fileDto = null;
						byte update_flag = 0x00;
				
						// 1. 버전체크 (하게되면 어느쪽에서 할 것인가 )
						
						
						JSONObject device_info = FirmwareManager.requestDeviceInfo(messageBean.device_id+"");
						if (device_info != null) {
							//System.out.println(device_info.toString());

							if (device_info.has("update_version") && device_info.has("device_type")
									&& device_info.has("update_enable")) {

								
								
								if (device_info.getBoolean("update_enable")) {
									byte[] binary = FirmwareManager.requestFirmwareBinaryBytes(device_info.getInt("device_type"),
											device_info.getString("update_version"));

									if (binary.length > 0) {
										String fileName = device_info.getString("update_version")+".binary";
										fileDto = getFileInfoDto(device_info.getString("update_version"),fileName,binary);
										firmware_map.put(socket, fileDto);
										update_flag = 0x01;
										
										// process
									} else {
										System.err.println("file not exist");
									}
								}
							}
						}else {
							System.err.println("device not exist");
					
						}
						
						
						
						if(fileDto != null) {
							responseBean.setMessage_type(response_type);
							responseBean.setDevice_id(messageBean.device_id);
							responseBean.setVersion(fileDto.getVersion());
							responseBean.setUpdate_flag(update_flag);
							responseBean.setFile_size(fileDto.getFile_size());
							responseBean.setFile_crc(fileDto.getFile_crc());
							responseBean.setMtu_size((short) 256);
							responseBean.setMtu_count(fileDto.getMtu_count());
						}else {
							
							// 업데이트 활성화가 안된 경우 or binary 파일을 찾지 못한 경우 
							responseBean.setMessage_type(response_type);
							responseBean.setDevice_id(messageBean.device_id);
							responseBean.setUpdate_flag(update_flag);
						}
						
						socket.send(responseBean.getBytes(messageBean.message_type));
						
						break;
					case MSG_TYPE_OTA_SEND_REQ:
						response_type = MSG_TYPE_OTA_SEND_RSP;
						if(firmware_map.get(socket) != null) {
							FileInfoDto file = firmware_map.get(socket);
							responseBean.setMessage_type(response_type);
							responseBean.setDevice_id(messageBean.device_id);
							responseBean.setVersion(file.getVersion());
							responseBean.setMtu_size((short) 256);
							responseBean.setMtu_index(messageBean.getMtu_index());
							responseBean.setCrc(fn_makeCRC16toShort(file.getFile_data().get(messageBean.getMtu_index())));
							responseBean.setData(file.getFile_data().get(messageBean.getMtu_index()));
						}
						
						socket.send(responseBean.getBytes(messageBean.message_type));
						break;
					case MSG_TYPE_OTA_FIN_REQ:
						response_type = MSG_TYPE_OTA_FIN_RSP;
						responseBean.setMessage_type(response_type);
						responseBean.setDevice_id(messageBean.device_id);
						socket.send(responseBean.getBytes(messageBean.message_type));
						
						firmware_map.remove(socket);
						
						break;
					default:
						System.out.println("default" + messageBean.message_type);
						break;
					}

				}

				@Override
				public void disconnect(TcpSocket socket) {
					if(firmware_map.get(socket) != null) {
						firmware_map.remove(socket);
					}
					// TODO Auto-generated method stub
					
				}

			});
			connectionManager.startServer();

		} else {
			System.out.println("plz input ur om2m_server_url & server.prop path");

		}

		
		// LoRa 
		//startMqttClient();

	}
	
	public static short fn_makeCRC16toShort(byte[] bytes) {
		short icrc = 0;
		
		for (byte b : bytes) {
			icrc += ((b & 0xff)); 
		}
		
		return (short)icrc; 
	}
	
	

	public static FileInfoDto getFileInfoDto(String version,String fileName, byte[] array) {

		InputStream is = new ByteArrayInputStream(array);

		FileInfoDto dto = new FileInfoDto();
		List<byte[]> file_data = new ArrayList<byte[]>();

		byte[] buf = new byte[DEFAULT_BUFFER_SIZE];
		int readBytes;
		short mtu_count = 0;

		short icrc = 0;

		try {
			while ((readBytes = is.read(buf)) > 0) {
//			fos.write(buf, 0, readBytes);

				byte[] new_buf = new byte[readBytes];
				System.arraycopy(buf, 0, new_buf, 0, readBytes);
				file_data.add(new_buf);

				for (byte b : new_buf) {
					icrc += ((b & 0xff));
				}

				mtu_count++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dto.setVersion(version);
		dto.setFile_name(fileName);
		dto.setFile_size(array.length);
		dto.setFile_crc(icrc);
		dto.setMtu_count(mtu_count);
		dto.setFile_data(file_data);
		
		return dto;
	}

	public static void startFileWatcherTimer() {
		TimerTask task = new FileWatcher(new File(PROPERTIE_FILE_LOCATION)) {

			@Override
			protected void onChange(File file) {
				// TODO Auto-generated method stub

				synchronized (lock) {
					System.out.println("File " + file.getName() + " have change !");
					setProp();
				}
			}
		};

		Timer timer = new Timer();
		timer.schedule(task, 0, 1000);
	}

	public static void setProp() {

		properties.clear();
		prop_map.clear();
		FileInputStream fis;
		try {
			fis = new FileInputStream(PROPERTIE_FILE_LOCATION);
			properties.load(fis);
			System.out.println("-- server propertieslist --");
			for (Object key : properties.keySet()) {
				String prop = properties.getProperty(key + "");
				if (prop != null) {
					String ip = prop.split(":")[0];
					String port = prop.split(":")[1];
					prop_map.put(Integer.parseInt(key + ""), new PropBean(ip, Short.parseShort(port)));
					System.out.println(key + ":" + properties.getProperty(key.toString()));
				}
			}
			System.out.println("---------------------------");
		} catch (Exception e) {
			System.out.println(e.getMessage()+"@@");
		}
	}

	public static PropBean getProp(int device_id) {
		Map.Entry<Integer, PropBean> entry = prop_map.floorEntry(device_id);
		return entry.getValue();
	}

	public static void startMqttClient() {

		mqttClient = new MQTT();
		mqttClient.setCallback(new MqttCallback() {
			// Message arrived :
			// {"applicationID":"4","applicationName":"XDOT-NA1-A00","deviceName":"XDOT-NA1-A00","devEUI":"00800000040175fe","rxInfo":[{"gatewayID":"60c5a8fffe7615d2","uplinkID":"775e3009-2bc3-489f-ba10-ef085502c603","name":"RAK7249","rssi":-66,"loRaSNR":8.5,"location":{"latitude":36.43181,"longitude":127.38991,"altitude":37}}],"txInfo":{"frequency":922300000,"dr":0},"adr":true,"fCnt":19,"fPort":1,"data":"MTExMQ=="}
			// Message arrived :
			// {"applicationID":"4","applicationName":"XDOT-NA1-A00","deviceName":"XDOT-NA1-A00","devEUI":"00800000040175fe","rxInfo":[{"gatewayID":"60c5a8fffe7615d2","uplinkID":"9f7a2219-8659-4d9d-8aa6-bb26d4622eb7","name":"RAK7249","rssi":-53,"loRaSNR":10,"location":{"latitude":36.4318,"longitude":127.38986,"altitude":13}}],"txInfo":{"frequency":922500000,"dr":0},"adr":true,"fCnt":0,"fPort":1,"data":"MTIzNA=="}

			@SuppressWarnings("unchecked")
			@Override
			public void messageArrived(String topic, MqttMessage mqttMessage) {

				try {
					String payload = new String(mqttMessage.getPayload(), "UTF-8");
					System.out.println(tiemStamp() + " Mqtt Message arrived / " + topic + " :: " + payload);

					if (payload == null || payload.trim().equals("")) {
						System.err.println("Mqtt Payload is empty");
						return;
					} else {
						JSONObject o = new JSONObject(payload);
						if (o.has("data")) {
							// System.out.println("Decode Base64 : "+ base64Decode(o.getString("data")));
						}

						String message = "11111111112222222222333333333344444444445555555555";
						downLink(topic, o, message);
					}

//					String last_topic_str = topic.substring(topic.lastIndexOf("/rx"), topic.length());
//
//					if (last_topic_str.contentEquals("/rx")) {
//					
//						downLink(topic, o);
//					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}

			@Override
			public void connectionLost(Throwable cause) {
				System.out.println("Mqtt Lost Connection." + cause.getCause());
				connect();
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
				System.out.println("Mqtt Message with " + iMqttDeliveryToken + " delivered.");
			}
		});

		connect();
	}

	public static void downLink(String topic, JSONObject o, String message) {
		String downlink_topic = topic.substring(0, topic.lastIndexOf("/")) + "/tx";
		JSONObject data = new JSONObject();
		data.put("confirmed", true);
		data.put("fPort", o.get("fPort"));
		data.put("data", base64Encode(message));
		mqttClient.publish(downlink_topic, data.toString());
	}

	public static String base64Encode(String source) {
		Encoder encoder = Base64.getEncoder();
		return new String(encoder.encode(source.getBytes()));
	}

	public static String base64Decode(String source) {
		Decoder decoder = Base64.getDecoder();
		return new String(decoder.decode(source.getBytes()));
	}

	public static void connect() {

		mqttClient.connect(new IMqttActionListener() {

			@Override
			public void onSuccess(IMqttToken asyncActionToken) {
				// TODO Auto-generated method stub
				System.out.println("Mqtt Connect onSuccess");
				mqttClient.subscribe();
			}

			@Override
			public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
				System.out.println("Mqtt Connect onFailure " + exception.getMessage());
				// TODO Auto-generated method stub

			}
		});
	}

}
