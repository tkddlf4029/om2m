package kbell;


public class ConnectionManager implements Constants{
	
	MessageCallback messageCallback;
	PacketParser packetParser;
	Server server;
	
	public ConnectionManager(MessageCallback messageCallback) {
		this.server = Server.getInst();
		this.packetParser = new PacketParser();
		this.messageCallback = messageCallback;
	}

	public void startServer() {
		 if(!server.isAlive()){ 
			 server.startExpScheduler(); // setSoTimeout 으로 자동으로 소켓을 종료 시킴
			 server.setMessageCallback(messageCallback); // Socket Callback
			 server.start(); 
		}
	}
}
