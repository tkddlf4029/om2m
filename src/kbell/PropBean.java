package kbell;

public class PropBean {
	

	String server_ip;
	short port;
	
	public PropBean() {}
	
	public PropBean(String server_ip, short port) {
		super();
		this.server_ip = server_ip;
		this.port = port;
	}
	
	
	

	//	String ip = "118.131.168.84";
	//  System.out.println(ipToLong(ip));
	//  System.out.println(longToIp(ipToLong(ip)));

	public int getIntServer_ip() {
		return ipToInt(server_ip);
	}
	
	public String getServer_ip() {
		return server_ip;
	}

	public void setServer_ip(String server_ip) {
		this.server_ip = server_ip;
	}

	public short getPort() {
		return port;
	}

	public void setPort(short port) {
		this.port = port;
	}
	
	
	// https://www.mkyong.com/java/java-convert-ip-address-to-decimal-number/
	public static int ipToInt(String ipAddress) {

		long result = 0;

		String[] ipAddressInArray = ipAddress.split("\\.");

		for (int i = 3; i >= 0; i--) {

			long ip = Long.parseLong(ipAddressInArray[3 - i]);

			// left shifting 24,16,8,0 and bitwise OR

			// 1. 192 << 24
			// 1. 168 << 16
			// 1. 1 << 8
			// 1. 2 << 0
			result |= ip << (i * 8);

		}

		return (int)result;
	}

	

	public static String intToIp(int i) {
		return ((i >> 24) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + (i & 0xFF);

	}

	@Override
	public String toString() {
		return "PropBean [server_ip=" + server_ip + ", port=" + port + "]";
	}
	
	

	
	
	
}
