package kbell;


public interface MessageCallback {
	
	void messageRecv(TcpSocket socket, MessageBean messageBean);
	void disconnect(TcpSocket socket);
}
