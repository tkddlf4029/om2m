package kbell;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

public class FirmwareManager {

	
	public static final String SERVER_ADDRESS = "http://192.168.1.152:8080/";

	@SuppressWarnings("finally")
	public static JSONObject requestDeviceInfo(String device_id) {
		HttpURLConnection con = null;
		BufferedReader br = null;
		JSONObject json = null;
		try {
			URL url = new URL(SERVER_ADDRESS+"/api/device_info?device_id="+device_id);

			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setConnectTimeout(3000);
			con.setReadTimeout(3000);
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setDefaultUseCaches(false);

			StringBuilder sb = new StringBuilder();
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line).append("\n");
				}
				//socket.send("OM2M HTTP Response  : " + sb.toString());
				if(!sb.toString().equals("")) {
					json = new JSONObject(sb.toString());
				}
				
			} else {
				//socket.send("OM2M HTTP Response : " + con.getResponseMessage());
				System.out.println("Response : "+ con.getResponseMessage());
			}

		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {

			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (con != null) {
				con.disconnect();
			}

			return json;
		}
	}
	
	@SuppressWarnings("finally")
	public static byte[] requestFirmwareBinaryBytes(int device_type , String version) {
		HttpURLConnection con = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		try {
			URL url = new URL(SERVER_ADDRESS+"/api/firmware_binary?device_type="+device_type+"&version="+version);

			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setConnectTimeout(3000);
			con.setReadTimeout(3000);
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setDefaultUseCaches(false);

			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				InputStream is = con.getInputStream();

				int nRead;
				byte[] data = new byte[1024];
				buffer = new ByteArrayOutputStream();

				while ((nRead = is.read(data, 0, data.length)) != -1) {
					buffer.write(data, 0, nRead);
				}

				// socket.send("OM2M HTTP Response : " + sb.toString());
				System.out.println("SIZE : " + buffer.toByteArray().length);
			} else {
				// socket.send("OM2M HTTP Response : " + con.getResponseMessage());
				System.out.println("ERR : " + con.getResponseMessage());
			}

		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {

			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (con != null) {
				con.disconnect();
			}

			return buffer.toByteArray();
		}
	}
}
