package kbell;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class MessageBean implements Constants {

	// header
	byte startByte1; // 0xFE
	byte startByte2; // 0xEF
	byte message_type;
	byte payload_length;

	// om2m
	int ty;

	// om2m
	int resource_id;

	// om2m & ota
	int device_id;

	// om2m 
	double latitude;
	double longitude;
	int temperature;
	int humidity;
	long PM10_0;
	long PM2_5;
	long PM1_0;
	float O3;
	float NO2;
	float SO2;
	byte Battery;
	byte Fall_Flag;
	long Fall_Timestamp;
	double Fall_latitude;
	double Fall_longitude;
	
	//ota 
	String version; 
	private short mtu_size;
	private short mtu_index;
	

	boolean swap = true;

	public MessageBean() {
		
        
	}

	public MessageBean(byte[] message) {

		try {

			ByteArrayInputStream bis = new ByteArrayInputStream(message);
			DataInputStream dis = new DataInputStream(bis);

			// ByteBuffer buf = ByteBuffer.wrap(message);
			// buf.order(ByteOrder.BIG_ENDIAN);
			// buf.order(ByteOrder.LITTLE_ENDIAN);

			// header
			startByte1 = dis.readByte();
			startByte2 = dis.readByte();
			message_type = dis.readByte();
			payload_length = dis.readByte();

			switch (message_type) {
			case MSG_TYPE_DEV_REG_REQ: // 12byte ( header[4] + body[8] )
				
				this.ty = TY_AE;
				if (swap) {
					this.resource_id = swap(dis.readInt());
					this.device_id = swap(dis.readInt());
				} else {
					this.resource_id = dis.readInt();
					this.device_id = dis.readInt();
				}

				break;

			case MSG_TYPE_SNC_CREATE_REQ: // 12byte ( header[4] + body[8] )
				
				this.ty = TY_CON;
				if (swap) {
					this.resource_id = swap(dis.readInt());
					this.device_id = swap(dis.readInt());
				} else {
					this.resource_id = dis.readInt();
					this.device_id = dis.readInt();
				}
				// this.sensor_type = dis.readInt();
				break;

			case MSG_TYPE_SN_INSERT_REQ: // 86byte ( header[4] + body[82] )
				
				this.ty = TY_CIN;
				if (swap) {
					this.resource_id = swap(dis.readInt());
					this.device_id = swap(dis.readInt());
					this.latitude = swap(dis.readDouble());
					this.longitude = swap(dis.readDouble());
					this.temperature = (int)swap(dis.readFloat());
					this.humidity = (int)swap(dis.readFloat());
//					this.PM10_0 = (int)swap(dis.readFloat());
//			    	this.PM2_5 = (int)swap(dis.readFloat());
//					this.PM1_0 = (int)swap(dis.readFloat());
					
					double t_PM10 = swap(dis.readFloat());
					double t_PM2_5 = swap(dis.readFloat());
					double t_PM1_0 = swap(dis.readFloat());
					
					System.out.println("PM10 : "+t_PM10+" / PM2.5 : "+t_PM2_5+" / PM1.0 : "+t_PM1_0);
					
					
					if(t_PM10<1) {
						this.PM10_0 = 1;
					}else {
						this.PM10_0 = (long)Math.round(t_PM10); // 반올림
					}
					
					if(t_PM2_5<1) {
						this.PM2_5 = 1;
					}else {
						this.PM2_5 = (long)Math.round(t_PM2_5);
					}
					
					
					if(t_PM1_0<1) {
						this.PM1_0 = 1;
					}else {
						this.PM1_0 = (long)Math.round(t_PM1_0);
					}
					
					this.O3 = swap(dis.readFloat()) * 0.1f;
					this.NO2 = swap(dis.readFloat()) * 0.1f; // 보정값
					this.SO2 = swap(dis.readFloat()) * 0.1f;
					this.Battery = dis.readByte(); 
					this.Fall_Flag = dis.readByte();
					this.Fall_Timestamp = swap(dis.readLong());
					this.Fall_latitude = swap(dis.readDouble());
					this.Fall_longitude = swap(dis.readDouble());
				} else {
					this.resource_id = dis.readInt();
					this.device_id = dis.readInt();
					this.latitude = dis.readDouble();
					this.longitude = dis.readDouble();
					this.temperature = (int)dis.readFloat();
					this.humidity = (int)dis.readFloat();
					this.PM10_0 = (long)Math.ceil(dis.readFloat());
					this.PM2_5 =(long)Math.ceil(dis.readFloat());
					this.PM1_0 =(long)Math.ceil(dis.readFloat());
					this.O3 = dis.readFloat();
					this.NO2 = dis.readFloat() * 0.1f;
					this.SO2 = dis.readFloat() * 0.1f;
					this.Battery = dis.readByte();
					this.Fall_Flag = dis.readByte();
					this.Fall_Timestamp = dis.readLong();
					this.Fall_latitude = dis.readDouble();
					this.Fall_longitude = dis.readDouble();
				}

				break;
			case MSG_TYPE_SRV_IP_REQ: // 12byte ( header[4] + body[8] )
				if (swap) {
					this.device_id = swap(dis.readInt());
				} else {
					this.device_id = dis.readInt();
				}

			case MSG_TYPE_OTA_INFO_REQ:
			case MSG_TYPE_OTA_FIN_REQ:
				this.device_id = swap(dis.readInt());
				this.version = swap(readString(dis, OTA_VERSION_NAEM_LENGTH));
				break;
			case MSG_TYPE_OTA_SEND_REQ:
				this.device_id = swap(dis.readInt());
				this.version = swap(readString(dis, OTA_VERSION_NAEM_LENGTH));
				this.mtu_size = swap(dis.readShort());
				this.mtu_index = swap(dis.readShort());
				break;
			default:
				break;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getCon() {
		List<String> con = new ArrayList<>();

		con.add(device_id+"");
		con.add(String.format("%.6f", latitude));
		con.add(String.format("%.6f", longitude));
		con.add(temperature + "");
		con.add(humidity + "");
		con.add(PM10_0 + "");
		con.add(PM2_5 + "");
		con.add(PM1_0 + "");
		con.add(O3 + "");
		con.add(NO2 + "");
		con.add(SO2 + "");
		con.add(Battery + "");
		con.add(Fall_Flag + "");
		con.add(Fall_Timestamp + "");
		con.add(Fall_latitude + "");
		con.add(Fall_longitude + "");

		return String.join(",", con);
	}

	public JSONObject getReqBody() {

		JSONObject data = new JSONObject();
		JSONObject body = new JSONObject();
		ArrayList<String> lbl = new ArrayList<String>();

		switch (message_type) {
		case MSG_TYPE_DEV_REG_REQ:

			// {“m2m:ae”:
			// {
			// “rn” : “sc_bic_”{{Device ID}},
			// “api” : “kb.2018.10.0001”,
			// “lbl” : [“key1”,”key2”],
			// “rr” : true
			// }
			// }

			data.put("rn", String.format("sc_bic_%04d", this.device_id));
			data.put("api", "kb.2018.10.0001");

			lbl.add("key1");
			lbl.add("key2");
			data.put("lbl", lbl);
			data.put("rr", true);

			body.put("m2m:ae", data);

			break;
		case MSG_TYPE_SNC_CREATE_REQ:

			// {“m2m:cnt”:
			// {
			// “rn” : {{Sensor Type}},
			// “lbl” : [“sensor”,”{{Sensor Type}}”]
			// }
			// }

			data.put("rn", "sensor");
			// data.put("rn", this.sensor_type);

			lbl.add("sensor");
			// lbl.add(this.sensor_type + "");
			data.put("lbl", lbl);

			body.put("m2m:cnt", data);

			break;

		case MSG_TYPE_SN_INSERT_REQ:

			// {“m2m:cin”:
			// {
			// “con” : {{Values}} // latitude ~ Fall_longitude 값을 “,”(콤마)로 구분한 값
			// }
			// }

			data.put("con", getCon());
			body.put("m2m:cin", data);

			break;
		}

		return body;
	}
	
	public short getMtu_size() {
		return mtu_size;
	}

	public void setMtu_size(short mtu_size) {
		this.mtu_size = mtu_size;
	}

	public short getMtu_index() {
		return mtu_index;
	}

	public void setMtu_index(short mtu_index) {
		this.mtu_index = mtu_index;
	}

	@Override
	public String toString() {
		return "OM2MBean [startByte1=" + startByte1 + ", startByte2=" + startByte2 + ", message_type=" + message_type
				+ ", payload_length=" + payload_length + ", ty=" + ty + ", resource_id=" + resource_id
				+ ", device_id=" + device_id + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", temperature=" + temperature + ", humidity=" + humidity + ", PM10_0="
				+ PM10_0 + ", PM2_5=" + PM2_5 + ", PM1_0=" + PM1_0 + ", O3=" + O3 + ", NO2=" + NO2 + ", SO2=" + SO2
				+ ", Battery=" + Battery + ", Fall_Flag=" + Fall_Flag + ", Fall_Timestamp=" + Fall_Timestamp
				+ ", Fall_latitude=" + Fall_latitude + ", Fall_longitude=" + Fall_longitude + ", getCon()=" + getCon()
				+ ", getReqBody()=" + getReqBody() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	private short swap(short x) {
		return (short) ((x << 8) | ((x >> 8) & 0xff));
	}

	private int swap(int x) {
		return (int) ((swap((short) x) << 16) | swap((short) (x >> 16)) & 0xffff);
	}

	private long swap(long x) {
		return (long) (((long) swap((int) (x)) << 32) | ((long) swap((int) (x >> 32)) & 0xffffffffL));
	}

	private float swap(float x) {
		return Float.intBitsToFloat(swap(Float.floatToRawIntBits(x)));
	}

	private double swap(double x) {
		return Double.longBitsToDouble(swap(Double.doubleToRawLongBits(x)));
	}
	
	private String swap(String x) {
		return new StringBuilder(x).reverse().toString();
	}
	
	private String readString(DataInputStream dis , int size) throws IOException {
		byte[] str = new byte[size];
		dis.read(str);
		return new String(str);
	}

}
