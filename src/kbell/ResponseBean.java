package kbell;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ResponseBean implements Constants {

	// header
	byte startByte1 = START_BYTE1; // 0xFE
	byte startByte2 = START_BYTE2; // 0xEF
	byte message_type;
	byte payload_length; // server_ip , port, return_Value

	// om2m
	int server_ip;
	short port;
	short return_value;

	// ota
	//private byte update_enable
	private int device_id;
	private String version;
	private byte update_flag;
	private int file_size;
	private short file_crc;
	private short mtu_size;
	private short mtu_count;
	
	private short mtu_index;
	private short crc;	
	private byte[] data;
	
	public ResponseBean() {
	}
	
	boolean swap = true;

	public byte[] getBytes(byte req_msg_type) {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);
		short ota_payload_length = 0; // ota의 경우 payload_length의 타입은 short이다
		
		try {
			
			
			dos.write(startByte1);
			dos.write(startByte2);
			dos.write(message_type);
			
			switch (req_msg_type) {
			case MSG_TYPE_DEV_REG_REQ:
			case MSG_TYPE_SNC_CREATE_REQ:
			case MSG_TYPE_SN_INSERT_REQ:
			case MSG_TYPE_SRV_IP_REQ:
				
				dos.writeShort(payload_length);
				dos.writeInt(server_ip);
				dos.writeShort(port);
				dos.writeShort(return_value);
				break;
				
				
			case MSG_TYPE_OTA_INFO_REQ:
				ota_payload_length = 8;
				dos.writeShort(ota_payload_length);
				
				dos.writeInt(device_id);
				if(version == null || version.length() > 0) {
					byte[] dummy = new byte[8];
					dos.write(dummy, 0, 8);
				}else {
					dos.write(version.getBytes(), 0, 8);
				}
				dos.write(update_flag);
				dos.writeInt(file_size);
				dos.writeShort(file_crc);
				dos.writeShort(mtu_size);
				dos.writeShort(mtu_count);
				
				break;
			case MSG_TYPE_OTA_SEND_REQ:
				// *** short 변경 필요 ***
				ota_payload_length = 8 ;  //data.length;
				
				dos.writeShort(ota_payload_length);
				dos.writeInt(device_id);
				
				if(version == null || version.length() > 0) {
					byte[] dummy = new byte[8];
					dos.write(dummy, 0, 8);
				}else {
					dos.write(version.getBytes(), 0, 8);
				}
				
				dos.writeShort(mtu_size);
				dos.writeShort(mtu_index);
				dos.writeShort(crc);	
				dos.write(data, 0, data.length);
			case MSG_TYPE_OTA_FIN_REQ:
				ota_payload_length = 8;
				dos.writeShort(ota_payload_length);
				dos.writeInt(device_id);
				
				if(version == null || version.length() > 0) {
					byte[] dummy = new byte[8];
					dos.write(dummy, 0, 8);
				}else {
					dos.write(version.getBytes(), 0, 8);
				}
				
			default:
				break;
			}

			

		} catch (IOException e) {
			System.out.print("@@@@@@@@@@2" + e.getMessage());
		}
		return bos.toByteArray();
	}
	
	

	public byte getStartByte1() {
		return startByte1;
	}



	public void setStartByte1(byte startByte1) {
		this.startByte1 = startByte1;
	}



	public byte getStartByte2() {
		return startByte2;
	}



	public void setStartByte2(byte startByte2) {
		this.startByte2 = startByte2;
	}



	public byte getMessage_type() {
		return message_type;
	}



	public void setMessage_type(byte message_type) {
		this.message_type = message_type;
	}



	public byte getPayload_length() {
		return payload_length;
	}



	public void setPayload_length(byte payload_length) {
		this.payload_length = payload_length;
	}



	public int getServer_ip() {
		return server_ip;
	}



	public void setServer_ip(int server_ip) {
		this.server_ip = server_ip;
	}



	public short getPort() {
		return port;
	}



	public void setPort(short port) {
		this.port = port;
	}



	public short getReturn_value() {
		return return_value;
	}



	public void setReturn_value(short return_value) {
		this.return_value = return_value;
	}



	public int getDevice_id() {
		return device_id;
	}



	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}



	public String getVersion() {
		return version;
	}



	public void setVersion(String version) {
		this.version = version;
	}
	
	



	public byte getUpdate_flag() {
		return update_flag;
	}



	public void setUpdate_flag(byte update_flag) {
		this.update_flag = update_flag;
	}



	public int getFile_size() {
		return file_size;
	}



	public void setFile_size(int file_size) {
		this.file_size = file_size;
	}



	public short getFile_crc() {
		return file_crc;
	}



	public void setFile_crc(short file_crc) {
		this.file_crc = file_crc;
	}



	public short getMtu_size() {
		return mtu_size;
	}



	public void setMtu_size(short mtu_size) {
		this.mtu_size = mtu_size;
	}



	public short getMtu_count() {
		return mtu_count;
	}



	public void setMtu_count(short mtu_count) {
		this.mtu_count = mtu_count;
	}



	public boolean isSwap() {
		return swap;
	}



	public void setSwap(boolean swap) {
		this.swap = swap;
	}

	


	public short getMtu_index() {
		return mtu_index;
	}



	public void setMtu_index(short mtu_index) {
		this.mtu_index = mtu_index;
	}



	public short getCrc() {
		return crc;
	}



	public void setCrc(short crc) {
		this.crc = crc;
	}
	
	



	public byte[] getData() {
		return data;
	}



	public void setData(byte[] data) {
		this.data = data;
	}



	@Override
	public String toString() {
		return "ResponseBean [startByte1=" + startByte1 + ", startByte2=" + startByte2 + ", message_type="
				+ message_type + ", payload_length=" + payload_length + ", server_ip=" + server_ip + ", port=" + port
				+ ", return_value=" + return_value + ", swap=" + swap + "]";
	}

	private short swap(short x) {
		return (short) ((x << 8) | ((x >> 8) & 0xff));
	}

	private char swap(char x) {
		return (char) ((x << 8) | ((x >> 8) & 0xff));
	}

	private int swap(int x) {
		return (int) ((swap((short) x) << 16) | swap((short) (x >> 16)) & 0xffff);
	}

	private long swap(long x) {
		return (long) (((long) swap((int) (x)) << 32) | ((long) swap((int) (x >> 32)) & 0xffffffffL));
	}

	private float swap(float x) {
		return Float.intBitsToFloat(swap(Float.floatToRawIntBits(x)));
	}

	private double swap(double x) {
		return Double.longBitsToDouble(swap(Double.doubleToRawLongBits(x)));
	}

}
